<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compitable" content="ie=edge">
    <meta name= "Description" content="Enter your description here"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Calculate</title>
</head>
<body>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <h1>Calculation</h1>
          <hr>
        <form>
  <div methods="post" action="">
    <label for="$num_voltage">Voltage</label>
    <input type="text" class="form-control" id="$num_voltage" name="$num_voltage">
    <label for="$num_current">Current</label>
    <input type="text" class="form-control" id="$num_current" name="$num_current">
    <label for="num_current rate">Current rate</label>
    <input type="text" class="form-control" id="$num_current rate" name="$num_current rate">
  </div>
  
  <button type="submit" class="btn btn-primary">calculate</button>
</form>
<?php
 function cal_power($num_voltage, $num_current) {
  $count1 = $num_voltage * $num_current;
  $count = number_format($count1, 0);
  return $count;
 }
  function cal_energy($num_power, $num_hour) {
  $count2 = $num_power * $num_hour*1000;
  $count = number_format($count2, 0);
  return $count;
 }
   function cal_total($num_energy, $num_currentrate) {
  $count3 = $num_energy * ($num_currentrate/100);
  $count = number_format($count3, 0);
  return $count;
 }

echo "Power : ".cal_power(15, 2.14);
echo "Energy: ".cal_energy(32,1);
echo "Total : ".cal_total(32,19.80);
?>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
</body>
</html>